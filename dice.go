package main

import (
	"fmt"
	"math/rand"
	"time"
)

func roll(sides int) int {
	rand.Seed(time.Now().UnixNano())
	randomNumber := rand.Intn(sides) + 1
	return randomNumber
}

func main() {
	dice, sides := 2, 6
	rolls := 2
	
	for r := 1; r <= rolls; r++ {
		sumOfDiceRoll := 0
		for d := 1; d <= dice; d++ {
			rolled := roll(sides)
			sumOfDiceRoll += rolled
			fmt.Println("Roll #", r, ", Die #", d, ":", rolled)
		}
		fmt.Println("Total rolled:", sumOfDiceRoll)
		switch sumOfDiceRoll := sumOfDiceRoll; {
		case sumOfDiceRoll == 2 && dice == 2:
			fmt.Println("Snake Eyes")
		case sumOfDiceRoll == 7:
			fmt.Println("Lucky 7")
		case sumOfDiceRoll % 2 == 0:
			fmt.Println("Even")
		case sumOfDiceRoll % 2 != 0:
			fmt.Println("Odd")
		}
	}

	

}


